import { fizzbuzz } from "../src/fizzbuzz"

// Vous devez construire une fonction fizzBuzz qui retourne un nombre ou une chaîne de caractère selon les règles suivantes :

// Si le nombre est divisible par 3, on le remplace par “Fizz” ;
// Si le nombre est divisible par 5, on le remplace par “Buzz” ;
// Si le nombre est divisible par 3 et 5, alors on le remplace par “FizzBuzz” ;
// Par défaut, retourner le nombre.

describe("Fizz", () => {
  test("default case", () => {
    expect(fizzbuzz(1)).toBe(1)
    expect(fizzbuzz(2)).toBe(2)
  })

  test("Should return Fizz when input is multiple of 3", () => {
    expect(fizzbuzz(3)).toBe("Fizz")
    expect(fizzbuzz(6)).toBe("Fizz")
  })

  test("Should return Buzz when input is multiple of 5", () => {
    expect(fizzbuzz(5)).toBe("Buzz")
    expect(fizzbuzz(10)).toBe("Buzz")
  })

  test('Shoud return "FizzBuzz" when input is multiple of 3 and 5', () => {
    expect(fizzbuzz(15)).toBe("FizzBuzz")
    expect(fizzbuzz(30)).toBe("FizzBuzz")
    expect(fizzbuzz(0)).toBe("FizzBuzz")
  })
})
