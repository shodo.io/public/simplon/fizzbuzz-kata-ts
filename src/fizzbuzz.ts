export const fizzbuzz = (nb: number) => {
  const isNbMultipleOf = isMultipleOf(nb)
  if (isNbMultipleOf(3) && isNbMultipleOf(5)) return "FizzBuzz"
  if (isNbMultipleOf(3)) return "Fizz"
  if (isNbMultipleOf(5)) return "Buzz"
  return nb
}

const isMultipleOf = (nb: number) => (multiple: number) => nb % multiple === 0
